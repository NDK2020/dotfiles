#![allow(non_snake_case)]
mod rules;
use std::{fs::File, path::PathBuf};
use std::io::Write;
// use std::io::prelude::*;
// use std::path::Path;
use serde_json::{json, Value as JsonValue};


fn main() {
  println!("\n************ BEGIN... ************\n");

  writeRules();

  println!("\n************ END!!... ************\n");
}


// const KARABINER_PATH: &str = "$HOME/.config/karabiner/karabiner.json";
// const KARABINER_PATH: &str = "/Users/khoa.nd/.config/karabiner/karabiner.json";
const KARABINER_PATH: &str = ".config/karabiner/karabiner.json";
fn writeRules() {

  let mut rules =  generateRules(&rules::getRules());

  let r = vec![rules];
  let config = json!({
    "global": {
      "show_in_menu_bar": false
    },
    "profiles": [
      {
        "name": "default",
        "complex_modifications": {
          "rules": r
        }
      }
    ]
  });


  let h = dirs::home_dir();
  let mut karabinerPath: PathBuf;

  if h.is_some() {
    let mut karabinerPath: PathBuf = h.unwrap();
    karabinerPath.push(KARABINER_PATH);
    /* let mut f = File::create(karabinerPath)
    .expect("can't create file");
    f.write_all(config.to_string().as_bytes()).expect("could not write"); */
  }

  
  println!("{}", config["profiles"][0]["complex_modifications"]["rules"]);
  // println!("{}", rules);

}

fn generateRules(s: &str) -> String {
  if !checkJsonFormat(s) {
    return "".to_string();
  }
  return s.to_string();
}

fn checkJsonFormat(x: &str) -> bool {
  let val: Result<JsonValue, serde_json::Error> = serde_json::from_str(x);
  return val.is_ok();
}
