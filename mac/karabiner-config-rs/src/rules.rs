#![allow(non_upper_case_globals)]
use std::any;

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
struct KarabinerRules {
  description: Option<String>,
  manipulator: Options<Manipulator[]>
}

#[derive(Serialize, Deserialize)]
struct Manipulator {
  description: String,
  type: "basic",
  from: any,
  to: any,
  to_after_key_up: any,
  to_if_alone: any,
  parameters: Parameters,
  conditions: any;
}

#[derive(Serialize, Deserialize)]
struct Parameter {
  "basic.simultaneous_threshold_milliseconds".to_string(): Option<i32>
}

/*
#[derive(Serialize, Deserialize)]
struct From {
  key_code?: KeyCode;
  simultaneous?: SimultaneousFrom[];
  simultaneous_options?: SimultaneousOptions;
  modifiers?: Modifiers;
}


#[derive(Serialize, Deserialize)]
struct Modifiers {
  optional: Option<String[]>,
  mandatory: Option<String[]>
}


#[derive(Serialize, Deserialize)]
struct To {
  key_code: any;
  modifiers: Option<String[]>,
  shell_command: Option<String>,
  set_variable: {
    name: string,
    value: any,
  },
  mouse_key: any;
} */

pub fn getRules() -> String {

  let mut rules: String = "".to_string();
  rules = [
    HyperKey.to_string(),
  ].join(",");

  // rules = format!("{{\n {} \n}}", rules);

  return rules;
}

pub const HyperKey: &str = r#"
  {
    "description": "Hyper Key (⌃⌥⇧⌘)",
    "manipulators": [
      {
        "type": "basic",
        "description": "Caps Lock -> Hyper Key",
        "from": {
          "key_code": "caps_lock"
        },
        "to": [
          {
            "key_code": "left_shift",
            "modifiers": ["left_command", "left_control", "left_option"]
          }
        ],
        "to_if_alone": [
          {
            "key_code": "escape"
          }
        ]
      }
    ]
  }"#;

const NextKey: &str = r#"
{
  "description": "Hyper Key (⌃⌥⇧⌘)",
  "manipulators": [
    {
      "type": "basic",
      "description": "Caps Lock -> Hyper Key",
      "from": {
        "key_code": "caps_lock"
      },
      "to": [
        {
          "key_code": "left_shift",
          "modifiers": ["left_command", "left_control", "left_option"]
        }
      ],
      "to_if_alone": [
        {
          "key_code": "escape"
        }
      ]
    }
  ]
}"#;

/* fn main() {

} */
