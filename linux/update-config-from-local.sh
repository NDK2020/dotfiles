#!/bin/bash

pathConfig="$HOME/.config"
# pathNvim="/nvim/"
pathKitty="/kitty/"
pathCoc="/coc/"
pathStarship="/starship.toml"
arr=(${pathKitty} ${pathCoc} ${pathStarship})

# remember to add permission: sudo chmod +x index.sh
to='../common/'
for from in "${arr[@]}"
do
  cp -r ${pathConfig}${from} $to
  # echo ${pathConfig}${from}
done

echo "finish copy"
