﻿" option to enable plugins
set nocompatible  
filetype plugin indent on
syntax enable

" -----------------------------------------------------------------------------
"                                  @VimPlug 
" -----------------------------------------------------------------------------
" {{{
call plug#begin ('~/.vim/plugged') 
"colorscheme 

  Plug 'dracula/vim' 
  Plug 'drewtempelmeyer/palenight.vim' 
  Plug 'junegunn/seoul256.vim' 
  Plug 'pineapplegiant/spaceduck', { 'branch': 'main' }
  Plug 'jacoborus/tender.vim'
  Plug 'lifepillar/vim-wwdc16-theme' 
  Plug 'danilo-augusto/vim-afterglow' 
  Plug 'vietch2612/trueboy.vim'
  Plug 'bluz71/vim-moonfly-colors'
  Plug 'embark-theme/vim', { 'as': 'embark' }
  Plug 'glepnir/oceanic-material'
  Plug 'KabbAmine/yowish.vim'
  Plug 'kaicataldo/material.vim', { 'branch': 'main' }

  "compile and running
  Plug 'skywind3000/asyncrun.vim' 
  Plug 'skywind3000/asyncrun.extra' 
  Plug 'skywind3000/asynctasks.vim' 
  "Plug 'turbio/bracey.vim', {'do': 'npm install --prefix server'}
  Plug 'voldikss/vim-floaterm'

  "interfaces
  Plug 'itchyny/lightline.vim' 
  Plug 'ryanoasis/vim-devicons'  

  "git
  Plug 'stsewd/fzf-checkout.vim'
  Plug 'tpope/vim-fugitive' 
  Plug 'tpope/vim-rhubarb' 
  Plug 'shumphrey/fugitive-gitlab.vim'
  Plug 'itchyny/vim-gitbranch'
  Plug 'mhinz/vim-signify' 
  Plug 'junegunn/gv.vim'
  Plug 'rhysd/conflict-marker.vim'

  "management: search, view, tags etc...
  Plug 'junegunn/fzf', {'dir': '~/.fzf','do': './install --all'}
  Plug 'junegunn/fzf.vim'
  Plug 'zackhsi/fzf-tags'
  Plug 'yuki-yano/fzf-preview.vim', { 'rev': 'release/rpc' } 
  Plug 'dyng/ctrlsf.vim'
  Plug 'preservim/tagbar' 
  Plug 'liuchengxu/vista.vim'
  Plug 'mhinz/vim-startify' 
  Plug 'lambdalisue/fern.vim' 
  Plug 'lambdalisue/fern-renderer-devicons.vim' 
  Plug 'lambdalisue/glyph-palette.vim' 
  Plug 'wellle/visual-split.vim'
  Plug 'szw/vim-maximizer' 
  Plug 'kshenoy/vim-signature'
  "Plug 'liuchengxu/vim-clap'
  "Plug 'RRethy/vim-illuminate'
  Plug 'uptech/vim-ping-cursor'
  "Plug 'ludovicchabant/vim-gutentags' 
  "Plug 'pechorin/any-jump.vim'
  "Plug 'yuttie/comfortable-motion.vim'

  "Note
  Plug 'vimwiki/vimwiki' 
  "Plug 'plasticboy/vim-markdown' 
  Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'  }

  "programming language
  "Plug 'yuezk/vim-js' 
  "Plug 'maxmellon/vim-jsx-pretty' 
  "Plug 'HerringtonDarkholme/yats.vim'
  Plug 'sheerun/vim-polyglot'
  "Plug 'leafgarland/typescript-vim' 
  "Plug 'peitalin/vim-jsx-typescript' 
  Plug 'neoclide/coc.nvim', { 'rev': 'release', 'merged': 0 } 
  Plug 'antoinemadec/coc-fzf', {'branch': 'release'}
  Plug 'rhysd/vim-clang-format'

  "snippets
  Plug 'mattn/emmet-vim' 
  Plug 'tpope/vim-repeat' 
  "Plug 'SirVer/ultisnips' 
  "Plug 'honza/vim-snippets' 

  "Utilities
  Plug 'jdhao/better-escape.vim'
  "Plug 'tpope/vim-surround' 
  Plug 'machakann/vim-sandwich'
  Plug 'tpope/vim-commentary' 
  Plug 'tpope/vim-unimpaired'
  Plug 'Yggdroot/indentLine' 
  Plug 'luochen1990/rainbow'
  Plug 'andymass/vim-matchup' 
  Plug 'easymotion/vim-easymotion' 
  Plug 'andrewradev/splitjoin.vim' 
  Plug 'chaoren/vim-wordmotion'
  Plug 'wellle/targets.vim'
  Plug 'AndrewRadev/sideways.vim'
  Plug 'unblevable/quick-scope' 
  Plug 'junegunn/vim-easy-align'
  Plug 'AndrewRadev/deleft.vim' 
  Plug 'krcs/vim-movelines'
  Plug 'mapkts/enwise'
  Plug 'kana/vim-textobj-user'
  Plug 'kana/vim-textobj-line'
  Plug 'inside/vim-textobj-jsxattr'
  Plug 'mg979/vim-visual-multi', {'branch': 'master'}
  "Plug 'voldikss/vim-codelf'
  "Plug 'rlue/vim-barbaric'
  Plug 'kevinhwang91/vim-ibus-sw'
  " Plug 'KabbAmine/vCoolor.vim'
  Plug 't9md/vim-choosewin'
  Plug 'machakann/vim-highlightedyank'
  "terminal
  Plug 'knubie/vim-kitty-navigator'
  Plug 'puremourning/vimspector'
  call plug#end() 
"}}}

" -----------------------------------------------------------------------------
"                                  @Colorscheme 
" -----------------------------------------------------------------------------
" {{{
set t_Co=256
if exists('+termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif

"colorscheme dracula  
let g:material_terminal_italics = 1
"let g:material_theme_style = 'lighter'
colorscheme material
"colorscheme embark
"colorscheme tender
"colorscheme wwdc16
"colorscheme afterglow
"let g:afterglow_inherit_background=1 
"let g:afterglow_blackout=1
"colorscheme trueboy
"colorscheme moonfly
"let g:moonflyTransparent = 1
"---
"set background=dark
"colorscheme spaceduck
"colorscheme palenight
"---
"set background=light
"colorscheme seoul256-light
"colorscheme stellarized
" }}}


" -----------------------------------------------------------------------------
"                                  @Styling 
" -----------------------------------------------------------------------------

" {{{ 
" !!! set font !!!
" set font commands in vim don't work because of Kitty font setting
" set guifont=Iosevka_NF:h11
" set guifont=JetBrains_Mono_Semibold:h10
" set gfn=FantasqueSansMono\ Nerd\ Font\ Mono\ 14
" !!! end of set font !!!

" Diff Color
hi DiffAdd ctermbg=235 ctermfg=108 cterm=reverse guibg=#262626 guifg=#87af87 gui=reverse
hi DiffChange ctermbg=235 ctermfg=103 cterm=reverse guibg=#262626 guifg=#8787af gui=reverse
hi DiffDelete ctermbg=235 ctermfg=131 cterm=reverse guibg=#262626 guifg=#af5f5f gui=reverse
hi DiffText ctermbg=235 ctermfg=208 cterm=reverse guibg=#262626 guifg=#ff8700 gui=reverse
"!!! Highlight Element !!!

hi Comment cterm=italic guifg='LightGreen' 
"hi Comment cterm=bold ctermfg=LightGreen
"hi Normal guibg=NONE cterm=NONE ctermbg=NONE 
" highlight Folded ctermbg=red ctermfg=White
highlight LineNr guifg=#c2c1ff 
"favorite color
"yellow nuances:#fff7c6 
"green nuances:#e3ff3f 
"purple nuances: #c2c1ff

"blue nuances: #64fcc9
" set cursorline
" highlight CursorLine guibg=NONE ctermbg=NONE 
" highlight CursorLineNr cterm=NONE guibg=NONE guifg=#fff7c6
" autocmd ColorScheme * highlight! link SignColumn LineNr
" highlight SignColumn guibg=blue 

"Visual color
hi Visual guifg=#5432d3 guibg=#fdefef
"#fecc50
"#fdefef
"#241e92
"#5432d3
"#ff004d

"!!! End of Highlight Element !!!

" !!!!!!!!!!!!!!!!!! Mark text line has length > 80 !!!!!!!!!!!!!!!!!!!!!!!!!!!
" Method 1
" highlight column > 81, original color =#263238
" let &colorcolumn=join(range(81,999),",")
" highlight ColorColumn ctermbg=235 guibg=#28343b

" Method 2
" highlight in the character in magenta when the line goes over the 80-chars
" column
highlight ColorColumn ctermbg=magenta  guifg=#FF0075 guibg=#fff7c6
call matchadd('ColorColumn', '\%81v', 100)
" !!!!!!!!!!!!!!!!!! End of Mark text line has length > 80 !!!!!!!!!!!!!!!!!!!!
  "get highlightgroup under cursor
  function! Syn()
  for id in synstack(line("."), col("."))
    echo synIDattr(id, "name")
  endfor
endfunction
command! -nargs=0 Syn call Syn()

function! CurColor() 
  :echo synIDattr(synIDtrans(synID(line("."), col("."), 1)), "fg")
endfunction
command! -nargs=0 CC call CurColor()
"<-- END OF COLORSCHEME SETTING -->
"}}}

"  
" -----------------------------------------------------------------------------
"                                  @GeneralSetting 
" -----------------------------------------------------------------------------
" {{{
" !!! Cursor Shape !!!
" cursor shape ref chart
"  1 -> blinking block
"  2 -> solid block 
"  3 -> blinking underscore
"  4 -> solid underscore
"  5 -> blinking vertical bar
"  6 -> solid vertical bar
let &t_SI = "\<Esc>[3 q" "SI = INSERT mode
"let &t_SR = "\<Esc>[4 q" "SR = REPLACE mode
let &t_EI = "\<Esc>[2 q" "EI = NORMAL mode (ELSE) 
" !!! End of Cursor Shape Section !!!

set cmdheight=1
set encoding=utf-8
set fileencoding=utf-8
set history=1000
set wildmenu
set nohlsearch
set incsearch
set ignorecase
set smartcase
set ruler
"Initialize window size
"set lines=40 columns=90 this option make display false
set linebreak
set autoindent
set smartindent
set scrolloff=5
set showtabline=2
set expandtab tabstop=2 shiftwidth=2
autocmd FileType typescript,typescriptreact,scss setlocal stal=4 tabstop=4 shiftwidth=4 
set textwidth=80
set pumheight=20
"set splitbelow splitright
"set pythonthreedll=c:\Python37\python37.dll
"set pythonthreehome=c:\python37
set autoread
set laststatus=2
set noshowmode 
set autowriteall
"set backup
set nobackup
set nowritebackup
:packadd termdebug
let g:termdebug_wide = 1
set termwinsize=10x0
set timeoutlen=500
set foldmethod=marker
"set foldenable
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview
set noswapfile
"set directory^=$HOME/Work/vimswap//
set number
set relativenumber
set hidden
set mouse=a
"set noerrorbells
set showcmd "show me what I'm typing
set ttimeout ttimeoutlen=50
"!!! gui options, hide widgets !!!
" This section is for hiding unnecessary gui elements of gVim
set guioptions-=m "remove menubar
set guioptions-=T "remove toolbar
set guioptions-=r "remove right-hand scrollbar
set guioptions-=L "remove left-hand scrollbar
"Toggle menu bar F1, Toolbar F2, Right-hand scrollbar
nnoremap <C-F1> :if &go=~#'m'<Bar>set go-=m<Bar>else<Bar>set go+=m<Bar>endif<CR> 
nnoremap <C-F2> :if &go=~#'T'<Bar>set go-=T<Bar>else<Bar>set go+=T<Bar>endif<CR>
nnoremap <C-F3> :if &go=~#'r'<Bar>set go-=r<Bar>else<Bar>set go+=r<Bar>endif<CR>
nnoremap <C-F3> :if &go=~#'L'<Bar>set go-=L<Bar>else<Bar>set go+=L<Bar>endif<CR>
"!!! gui options, hide widgets !!!
"}}}


" -----------------------------------------------------------------------------
"                                  @VimMappings 
" -----------------------------------------------------------------------------

" {{{
" cwd - change current working directory
nnoremap <leader>cd :cd %:p:h<cr> :pwd<cr>

"!!! compile using vim native commands !!!
":nnoremap <C-S-b> :make<CR> 
":nnoremap <A-r> :!%:r<CR> 
":packadd termdebug
":noremap <S-F5> :Termdebug %:r<cr>
":nnoremap <leader>n :cnext<cr>
":nnoremap <leader>p :cprevious<cr>

" copy/paste clipboard
:nmap <S-p> "+p
:map <S-y> "+y
set pastetoggle=<F12>
":tnoremap <expr> <C-v> '<C-\><C-N>"'.nr2char(getchar()).'pi'
:tnoremap <leader>-c <C-W><C-c> 
inoremap <C-v> <F12><C-r>+<F12>

" exit 
nnoremap gq :q<cr>

" indenting entire file
:nnoremap <leader><leader>a gg=G``zz

" insert blank lines
" nnoremap <silent> ]<Space> :<C-u>put =repeat(nr2char(10),v:count)<Bar>execute "'[-1"<CR>
" nnoremap <silent> [<Space> :<C-u>put!=repeat(nr2char(10),v:count)<Bar>execute "']+1"<CR>

"---insert mode

""" Emacs shortcuts
" go to the start of the line
inoremap <C-a> <ESC>I
"nnoremap <C-a> ^
" go to the end of the line
inoremap <C-e> <ESC>A
nnoremap <C-e> $
" go back a word
execute "set <M-h>=\eh"
inoremap <M-h> <C-o>B
" go forward a word
execute "set <M-l>=\el"
inoremap <M-l> <C-o>W

"inoremap ;; <Esc>
"--- end of insert mode

" Navigation betweens tabs
nmap te :tabedit<Return>
" nnoremap <S-Tab> :tabprev<Return>
" nnoremap <Tab> :tabnext<Return>

" Naviation between window within a tab
"nmap <Space> <C-w>w
" nnoremap sh <C-W>h nnoremap sl <C-W>l
" nnoremap sj <C-W>j
" nnoremap sk <C-W>k
nnoremap so <C-W><C-o>
" nnoremap L :bnext<CR>
" nnoremap H :bprev<CR>
nmap sh :split<Return><C-w>w
nmap sv :vsplit<Return><C-w>w

" Select all
execute "set <M-a>=\ea"
nmap <M-a> gg<S-v>G

" Session
" exclamation point at the end will overwrite an existing session file if it
" exists
nnoremap <Leader>ms :mksession! ~/.vim/sessions/
nnoremap <Leader>os :source ~/.vim/sessions/
nnoremap <Leader>rs :!rm ~/.vim/sessions/

" !!! terminal map !!!
: "term ++kill=term
":tmap <S-p> <C-W>"+
" Switch to normal mode
:tmap <c-n> <C-W><S-n>
" exit terminal CTRL-D
tnoremap <c-d> <c-\><c-n>
" !!! end of terminal map !!!

"visual select text from most recently yanked
nnoremap [v `[V`]

"unmap, disable key
nmap s <Nop>
xmap s <Nop>
"<-- END OF Vim Mappings -->
"}}}


" -----------------------------------------------------------------------------
"                                  @Compile 
" -----------------------------------------------------------------------------

" {{{
execute "set <M-r>=\er"
execute "set <M-b>=\eb"
nnoremap <M-r> :call KittyRun()<Return> 
"nnoremap <M-b> :call KittyBuild()<Return> 
nnoremap <M-b> :call FtBuild()<Return> 

"!!! Build/ compile cpp program with native Vim features !!!
" set makeprg=g++\ -g\ -std=c++11\ -o\ %:r\ %  

"!!! Build/ compile cpp program with tmux !!!
" execute "set <M-b>=\eb"
" nnoremap <M-b> :call Build1()<Return> 
" let run1 = "tmux send-keys -t2 ./".expand("%:r")." Enter"
" execute "set <M-r>=\er"
" nnoremap <M-r> :call system(run1)<Return> 
" let t1 = "tmux send-keys -t 1 'clear' Enter"
" nnoremap <F8> :call system(t1)<Return>
""nnoremap <F10> :make<CR>
"function Build1() 
"    let comp1 = "tmux send-keys -t 1 "
"    let comp2 = "g++ -g -Wall -Wextra -Wshadow -std=c++11  "." -o ".expand("%:r")." ".expand("%")
"    let comp3 = comp1." '".comp2."' "."Enter"
"    "echo comp3 
"    write
"    call system(comp3)
"endfunction
"Setting to build program using Kitty terminal

"!!! Build/ compile cpp program with Kitty !!!

function KittySwitchTab() 
  let comp1 = "kitty @ focus-tab --match title:CompileThenRun"
  call system (comp1)
endfunction

function KittyBuild() 
  call KittySwitchTab()
  let comp1 = "kitty @ send-text --match title:build "
  let comp2 = "g++ -g -Wall -Wextra -Wshadow -fmax-errors=2 -fsanitize=address -D_GLIBCXX_DEBUG -D_GLIBCXX_DEBUG_PEDANTIC -D_GLIBCXX_ASSERTIONS -D_LOCAL -std=c++14 "." -o ".expand("%:r")." ".expand("%")
  let comp3 = comp1." '".comp2."' ".'\\'."x0d"
  write
  call system(comp3)
endfunction

function KittyRun()
  call KittySwitchTab()
  let run2 = "kitty @ send-text --match title:build ./"
  let run22 = expand("%:r")
  let run222 = ' \\'."x0d"
  let run2222= run2.run22.run222
  "echo run2222
  call system(run2222)
endfunction

"!!! End of Build/ compile cpp program with Kitty !!!

"!!! Build/ compile cpp program with Floaterm !!!
function FtBuild() 
  write
  let comp1 = "g++ -g -Wall -Wextra -Wshadow -fmax-errors=2 -fsanitize=address -D_GLIBCXX_DEBUG -D_GLIBCXX_DEBUG_PEDANTIC -D_GLIBCXX_ASSERTIONS -D_LOCAL -std=c++14 "." -o ".expand("%:r")." ".expand("%")

  let fileName = expand("%:r")
  ":FloatermNew --name=floaterm1 --position=topleft --autoclose=2 --cmd="cd ~"

  FloatermNew
  execute 'FloatermSend --autoclose=2 ' . comp1 . " && ./".fileName   

  " write
  " call system(comp3)
endfunction
"}}}


" -----------------------------------------------------------------------------
"                                  @KittySetting 
" -----------------------------------------------------------------------------

set title
let &titlestring='%t - nvim'


" =============================================================================
"                                  @Plugin Setting Section
" =============================================================================


" -----------------------------------------------------------------------------
"                                  @any-jump 
" -----------------------------------------------------------------------------
" {{{
"let g:any_jump_disable_default_keybindings = 1
" Normal mode: Jump to definition under cursor
"nnoremap <leader>j :AnyJump<CR>

" Visual mode: jump to selected text in visual mode
"xnoremap <leader>j :AnyJumpVisual<CR>

" Normal mode: open previous opened file (after jump)
" nnoremap <leader>ab :AnyJumpBack<CR>

" Normal mode: open last closed search window again
" nnoremap <leader>al :AnyJumpLastResults<CR>
" }}}


" -----------------------------------------------------------------------------
"                                  @asynctask 
" -----------------------------------------------------------------------------

" {{{
"open :AsyncTaskEdit! and paste content from our github
let g:asyncrun_open = 6
"noremap <silent><f10> :AsyncTask file-build<cr>
"noremap <silent> <F5> :AsyncRun -raw -cwd=$(VIM_FILEDIR) "$(VIM_FILEDIR)/$(VIM_FILENOEXT)" <cr>
let g:asynctasks_term_pos = 'external'
"}}}

" -----------------------------------------------------------------------------
"                                  @better-escape 
" -----------------------------------------------------------------------------
let g:better_escape_interval = 200
" let g:better_escape_shortcut = ['jk', 'JK']
let g:better_escape_shortcut = [';;']

" -----------------------------------------------------------------------------
"                                  @CtrlSF 
" -----------------------------------------------------------------------------

" {{{
"  " Set "<leader>s" to substitute the word under the cursor. Works great with
" CtrlSF!
nmap <leader>sf :%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>

" Set up some handy CtrlSF bindings
nmap <leader>cs :CtrlSF -R ""<Left>
nmap <leader>cw <Plug>CtrlSFCwordPath -W<CR>
nmap <leader>cf  :CtrlSFFocus<CR>
nmap <leader>ct  :CtrlSFToggle<CR>

" Use Ripgrep with CtrlSF for performance
let g:ctrlsf_ackprg = '/usr/bin/rg'
"}}}


" -----------------------------------------------------------------------------
"                                  @choosewin 
" -----------------------------------------------------------------------------
nmap  -  <Plug>(choosewin)

" -----------------------------------------------------------------------------
"                                  @easyalign 
" -----------------------------------------------------------------------------

" {{{
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)
"}}}

" -----------------------------------------------------------------------------
"                                  @easymotion 
" -----------------------------------------------------------------------------

" {{{
"nmap t <Plug>(easymotion-s2)
"nmap t <Plug>(easymotion-s)
nmap <leader>e <Plug>(easymotion-overwin-f)
"nmap mt <Plug>(easymotion-overwin-f2)
" }}}

" -----------------------------------------------------------------------------
"                                  @emmet-vim 
" -----------------------------------------------------------------------------

" {{{
let g:user_emmet_install_global = 0
let g:uset_emmet_mode = 'n'
let g:user_emmet_leader_key = ','
" check emmet filetype: :echo emmet#getFileType()
let g:user_emmet_settings = {
      \'typescriptreact': {
        \'default_attributes' : {
          \  'label': [{'htmlFor': ''}],
          \  'class': { 'className': ' '},
          \  },
          \},
          \'javascript': {
            \  'default_attributes': {
              \    'a': {'href': '#'},
              \    'link': [{'rel': 'what'}, {'href': ''}],
              \  },
              \  'attribute_name': {'for': 'htmlFor', 'class': 'className'},
              \},
              \}

autocmd Filetype css,html,javascript,javascriptreact,typescript,typescriptreact EmmetInstall 
"}}}

" -----------------------------------------------------------------------------
"                                  @enwise-vim 
" -----------------------------------------------------------------------------

"nnoremap <leader>te :EnwiseToggle<CR>
let g:enwise_enable_globally = 1


" -----------------------------------------------------------------------------
"                                  @fern.vim 
" -----------------------------------------------------------------------------

" {{{
"let g:fern#opener = "select
"let g:fern#renderer = "nerdfont"
let g:fern#renderer = "devicons"
let g:fern#default_hidden = 1
let g:fern_renderer_devicons_disable_warning = 1
let g:myRootDir = getcwd()
nnoremap <leader>d :Fern . -drawer -reveal=% -width=27 -toggle <cr><C-w>=

command! SetRoot call SetRootDir()
function SetRootDir()
  echo g:myRootDir
  exe "cd " . g:myRootDir
endfunction

function! s:init_fern() abort
  " Use 'select' instead of 'edit' for default 'open' action
  nmap <buffer><expr>
        \ <Plug>(fern-my-open-expand-collapse)
        \ fern#smart#leaf(
        \   "\<Plug>(fern-action-open)",
        \   "\<Plug>(fern-action-expand)",
        \   "\<Plug>(fern-action-collapse)",
        \ )
  nmap <buffer> <CR> <Plug>(fern-my-open-expand-collapse)
  nmap <buffer> <2-LeftMouse> <Plug>(fern-my-open-expand-collapse)
  nmap <buffer> <leader>cd <Plug>(fern-action-cd:cursor) 
  nmap <buffer> <leader>h <Plug>(fern-action-hidden:toggle) 
  nmap <buffer> <BS> <Plug>(fern-action-leave)
  nmap <buffer> mc <Plug>(fern-action-mark:clear)
  nmap <buffer> ma <Plug>(fern-action-mark:toggle)
  nmap <buffer> mv <Plug>(fern-action-move)
  "md = make dir = new dir
  nmap <buffer> md <Plug>(fern-action-new-dir) 
  nmap <buffer> mf <Plug>(fern-action-new-file) 
  nmap <buffer> mp <Plug>(fern-action-new-path) 
  nmap <buffer> df <Plug>(fern-action-remove)
  nmap <buffer> r <Plug>(fern-action-rename)
  nmap <buffer> sh <Plug>(fern-action-open:split)
  nmap <buffer> sv <Plug>(fern-action-open:side) 
  nmap <buffer> ss <Plug>(fern-action-open:select) 
  nmap <buffer> <c-h> <C-W>h
  nmap <buffer> <c-l> <C-W>l
  nmap <buffer> <c-j> <C-W>j
  nmap <buffer> <c-k> <C-W>k
  nmap <buffer><nowait> > <Plug>(fern-action-enter)
  nmap <buffer><nowait> < <Plug>(fern-action-leave)
  nmap <buffer> - <Plug>(choosewin)
endfunction

augroup fern-custom
  autocmd! *
  autocmd FileType fern call s:init_fern()
augroup END
" }}}


" !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"                              @GitPlugins Section 
" !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


" -----------------------------------------------------------------------------
"                                  @checkout 
" -----------------------------------------------------------------------------

nnoremap <leader>gc :GCheckout<CR>


" -----------------------------------------------------------------------------
"                                  @conflict-marker 
" -----------------------------------------------------------------------------
" disable the default highlight group
let g:conflict_marker_highlight_group = ''

" Include text after begin and end markers
let g:conflict_marker_begin = '^<<<<<<< .*$'
let g:conflict_marker_end   = '^>>>>>>> .*$'

highlight ConflictMarkerBegin guibg=#2f7366
highlight ConflictMarkerOurs guibg=#2e5049
highlight ConflictMarkerTheirs guibg=#344f69
highlight ConflictMarkerEnd guibg=#2f628e
highlight ConflictMarkerCommonAncestorsHunk guibg=#754a81
highlight ConflictMarkerSeparator guibg=#ff004d

" -----------------------------------------------------------------------------
"                                  @fugitive-vim 
" -----------------------------------------------------------------------------

nmap <leader>gb :G blame<cr>
nmap <leader>gd :Gvdiffsplit<cr>
nmap <leader>gdb :Gvdiffsplit :%<left><left>
nmap <leader>ge :Gedit<cr>
nmap <leader>gh :diffget //2<CR>
nmap <leader>gl :diffget //3<CR>
nmap <leader>gs :vertical G<CR>

" -----------------------------------------------------------------------------
"                                  @fugitive-gitlab 
" -----------------------------------------------------------------------------

let g:fugitive_gitlab_domains = ['https://my.gitlab.com']

" -----------------------------------------------------------------------------
"                                  @signify 
" -----------------------------------------------------------------------------

" {{{
" default updatetime 4000ms is not good for async update
" set updatetime=100
nmap ]h <plug>(signify-next-hunk)
nmap [h <plug>(signify-prev-hunk)
nmap <leader>st :SignifyToggle<cr>
nmap <leader>sd :SignifyHunkDiff<cr>
" nmap <leader>shd :SignifyDiff<cr>
" nmap <leader>gJ 9999<leader>gj
" nmap <leader>gK 9999<leader>gk

" If find the numbers distracting
" let g:signify_sign_show_count = 0
" let g:signify_sign_show_text = 1

" change sign
" let g:signify_sign_add               = '+'
" let g:signify_sign_delete            = '_'
" let g:signify_sign_delete_first_line = '‾'
" let g:signify_sign_change            = '!'

"Common Command
":SignifyToggle
":SignifyToggleHighlight
let g:signify_disable_by_default = 0
"}}}


" !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"                             @End of GitPlugins Section 
" !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


" -----------------------------------------------------------------------------
"                                  @gutentags 
" -----------------------------------------------------------------------------

" {{{
" manual command: ctags -R .
" let g:gutentags_trace = 1
" let g:gutentags_add_default_project_roots = 0
" let g:gutentags_project_root = ['package.json', '.git', 'root', '.svn', '.hg', '.project']
" let g:gutentags_cache_dir = expand('~/.cache/vim/ctags/')
" command! -nargs=0 GutentagsClearCache call system('rm ' . g:gutentags_cache_dir . '/*')
" let g:gutentags_generate_on_new = 1
" let g:gutentags_generate_on_missing = 1
" let g:gutentags_generate_on_write = 1
" let g:gutentags_generate_on_empty_buffer = 0
" let g:gutentags_ctags_extra_args = [ '--tag-relative=yes', '--fields=+ailmnS']
"let g:gutentags_ctags_exclude = [ '*.git', '*.svg', '*.hg', '*/tests/*', 'build', 'dist', '*sites/*/files/*', 'bin', 'node_modules', 'bower_components', 'cache', 'compiled', 'docs', 'example', 'bundle', 'vendor', '*.md', '*-lock.json', '*.lock', '*bundle*.js', '*build*.js', '.*rc*', '*.json', '*.min.*', '*.map', '*.bak', '*.zip', '*.pyc', '*.class', '*.sln', '*.Master', '*.csproj', '*.tmp', '*.csproj.user', '*.cache', '*.pdb', 'tags*', 'cscope.*', '*.css', '*.less', '*.scss', '*.exe', '*.dll', '*.mp3', '*.ogg', '*.flac', '*.swp', '*.swo', '*.bmp', '*.gif', '*.ico', '*.jpg', '*.png', '*.rar', '*.zip', '*.tar', '*.tar.gz', '*.tar.xz', '*.tar.bz2', '*.pdf', '*.doc', '*.docx', '*.ppt', '*.pptx', ]
" if executable('rg')
"   let g:gutentags_file_list_command = 'rg --files'
" endif
" }}}


" -----------------------------------------------------------------------------
"                                  @floaterm 
" -----------------------------------------------------------------------------

" {{{
" nnoremap   <silent>   <leader>ft    :FloatermNew echo "Hello World"<CR>{{{
" tnoremap   <silent>   <F7>    <C-\><C-n>:FloatermNew<CR>
" nnoremap   <silent>   <F8>    :FloatermPrev<CR>
" tnoremap   <silent>   <F8>    <C-\><C-n>:FloatermPrev<CR>
" nnoremap   <silent>   <F9>    :FloatermNext<CR>
" tnoremap   <silent>   <F9>    <C-\><C-n>:FloatermNext<CR>
nnoremap   <silent>   <leader>t   :FloatermToggle<CR>
"tnoremap   <leader>t   <C-\><C-n>:FloatermToggle<CR>:FloatermKill<CR>
tnoremap   <leader>t   <C-\><C-n>:FloatermToggle<CR>
nnoremap   <leader>k   :FloatermKill!<CR>
" tnoremap <leader>k :FloatermKill!<CR>
"}}}


" -----------------------------------------------------------------------------
"                                  @fzf, fzf-preview 
" -----------------------------------------------------------------------------

" {{{
"let g:fzf_preview_window = ['right:50%', 'ctrl-/']
"let g:fzf_preview_window = []
" FZF
let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }
hi fzfBorderColor guifg=#00FF00 guibg=#FFFF00 ctermbg=NONE ctermfg=NONE cterm=NONE
hi fzfNoneColor guifg=NONE guibg=NONE ctermbg=NONE ctermfg=NONE cterm=NONE
hi fzfVisualLine  guibg=#4B5D67 ctermfg=13

let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'fzfVisualLine'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'fzfBorderColor'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }

" let g:fzf_colors = { 'border': ['fg', 'fzfBorderColor'] }
let g:fzf_layout = {'up':'~90%', 'window': { 'width': 0.8, 'height': 0.8,'yoffset':0.5,'xoffset': 0.5, 'border': 'sharp' } }
let g:fzf_tags_command = 'ctags -R'

let $FZF_DEFAULT_OPTS = '--layout=reverse --inline-info'
let $FZF_DEFAULT_COMMAND = "rg --files --hidden --glob '!.git/**' --glob '!build/**' --glob '!.dart_tool/**' --glob '!.idea' --glob '!node_modules' --glob '!yarn.lock'"

command! -bang -nargs=? -complete=dir Files
      \ call fzf#run(fzf#wrap('files', fzf#vim#with_preview({ 'dir': <q-args>, 'sink': 'e', 'source': 'rg --files --hidden' }), <bang>0))
nnoremap <C-p> :Files<cr>


" Add an AllFiles variation that ignores .gitignore files
command! -bang -nargs=? -complete=dir AllFiles
      \ call fzf#run(fzf#wrap('allfiles', fzf#vim#with_preview({ 'dir': <q-args>, 'sink': 'e', 'source': 'rg --files --hidden --no-ignore' }), <bang>0))
execute "set <M-p>=\ep"
nnoremap <M-p> :AllFiles<cr>
nnoremap <leader>rg :Rg<cr>
nnoremap <leader>b :Buffers<cr>
nmap // :BLines<CR>
nmap ?? :Rg<CR>
":export FZF_DEFAULT_OPTS='--height 40% --layout=reverse --border'
"}}}


" -----------------------------------------------------------------------------
"                                  @indentline 
" -----------------------------------------------------------------------------

" {{{
"let g:indentLine_char = '|'
"let g:indentLine_setConceal = 0
"let g:indentLine_fileTypeExclude = ['coc-explorer']
"let g:indentLine_char = '│' 
let g:indentLine_char = '▏' 
"let g:indentLine_color_gui = '#9a8194'
let g:indentLine_color_gui = '#5b6d5b'
"let g:indentLine_color_gui = '#8e7f7f'
"let g:indentLine_color_term = 31 
" let g:indentLine_bgcolor_term = 202
" let g:indentLine_bgcolor_gui = '#FF5F00'
" let g:indentLine_color_tty_light = 7 " (default: 4)
" let g:indentLine_color_dark = 1 " (default: 2) 
" If you want to highlight conceal color with your colorscheme, disable by:
"let g:indentLine_setColors = 0
"}}}


" -----------------------------------------------------------------------------
"                                  @jsx-typescript 
" -----------------------------------------------------------------------------

" {{{
"set filetypes as typescriptreact
"autocmd BufNewFile,BufRead *.jsx,*.tsx set filetype=typescriptreact
"autocmd BufNewFile,BufRead *.js set filetype=typescript
"autocmd BufNewFile,BufRead *.jsx set filetype=javascript.tsx
"}}}


" -----------------------------------------------------------------------------
"                                  @lightline 
" -----------------------------------------------------------------------------

" {{{
if has('gui_running')
  set guioptions-=e
endif

let g:lightline = {}
" let g:lightline.colorscheme = "selenized_black"
let g:lightline.colorscheme = "material_vim"
let g:lightline.active = {
      \ 'left': [['mode','paste'], 
      \          ['fugitive', 'readonly',
      \           'relativepath' , 'modified',
      \           'devicons_filetype']],
      \ 'right': [['lineinfo']
      \ ,['linter_warnings','linter_errors','linter_ok']]
      \ }                     

let g:lightline.inactive = {
      \ 'left': [ [ 'fugitive','relativepath' , 'modified' ]],
      \ 'right': [ [ 'modified','filename', 'lineinfo' ] ]
      \ }

let g:lightline.tabline = {
      \ 'left': [ [ 'topleft_logo', 'tabs'] ],
      \ 'right': [ [ 'fugitiveForTab' ] ],
      \ }

let g:lightline.tab = {
      \ 'active': [ 'tabnum','tabpath', 'readonly','modified'],
      \ 'inactive': [ 'tabnum','tabpath', 'readonly','modified']
      \}

let g:lightline.tab_component_function = {
      \ 'tabnum': 'TabNum',
      \ 'filename': 'lightline#tab#filename',
      \ 'tabpath': 'TabPath',
      \ 'readonly': 'lightline#tab#readonly',
      \ 'modified': 'lightline#tab#modified',
      \ }

let g:lightline.component = {
      \ 'git_buffer' : '%{get(b:, "coc_git_status", "")}',
      \ 'git_global' : '%{GitGlobal()}',
      \ 'vim_logo': "\ue7c5",
      \ 'topleft_logo': "",
      \ 'lineinfo': '%2l:%-2v  %2p%%',
      \ }

let g:lightline.component_function = {
      \ 'devicons_filetype': 'DeviconsFiletype',
      \ 'devicons_only': 'DeviconsOnly',
      \ 'coc_status': 'CocStatus',
      \ 'gitbranch': 'gitbranch#name',
      \ 'fugitive': 'LightlineFugitive',
      \ 'fugitiveForTab': 'LightlineFugitiveForTab',
      \ }

let g:lightline.component_expand = {
      \ 'linter_warnings': 'CocDiagnosticWarning',
      \ 'linter_errors': 'CocDiagnosticError',
      \ 'linter_ok': 'CocDiagnosticOK',
      \ }

let g:lightline.component_type = {
      \ 'linter_warnings': 'warning',
      \ 'linter_errors': 'error'
      \ }

"linelight separator
" let g:lightline.separator = { 'left': '', 'right': '' }
" let g:lightline.subseparator = { 'left': '', 'right': '' }
let g:lightline.separator = { 'left': "\ue0b4", 'right': "\ue0b6" }
let g:lightline.subseparator = { 'left': "\ue0b5", 'right': "\ue0b7" }
let g:lightline.tabline_separator = { 'left': "\ue0bc", 'right': "\ue0ba" }
let g:lightline.tabline_subseparator = { 'left': "\ue0bb", 'right': "\ue0bb" }

"lightline transparent middle
let s:palette = g:lightline#colorscheme#{g:lightline.colorscheme}#palette
let s:palette.normal.middle = [ [ 'NONE', 'NONE', 'NONE', 'NONE' ] ]
let s:palette.inactive.middle = s:palette.normal.middle
let s:palette.tabline.middle = s:palette.normal.middle
" call insert(s:palette.normal.right, s:palette.normal.left[1], 0)

"""" lightline function implementation
function! CocDiagnosticError() abort 
  let info = get(b:, 'coc_diagnostic_info', {})
  return get(info, 'error', 0) ==# 0 ? '' : "\uf00d" . info['error']
endfunction 

function! CocDiagnosticWarning() abort 
  let info = get(b:, 'coc_diagnostic_info', {})
  return get(info, 'warning', 0) ==# 0 ? '' : "\uf529" . info['warning']
endfunction 

function! CocDiagnosticOK() abort 
  let info = get(b:, 'coc_diagnostic_info', {})
  if get(info, 'error', 0) ==# 0 && get(info, 'error', 0) ==# 0
    let msg = "\uf00c"
  else
    let msg = ''
  endif
  return msg
endfunction 

function! CocStatus() abort 
  return get(g:, 'coc_status', '')
endfunction 

function! GitGlobal() abort 
  let status = get(g:, 'coc_git_status', '')
  return status ==# '' ? "\ue61b" : status

endfunction 

function! DeviconsFiletype() 
  " return winwidth(0) > 70 ? (strlen(&filetype) ? WebDevIconsGetFileTypeSymbol() . ' ' . &filetype : 'no ft') : ''
  return winwidth(0) > 70 ? (strlen(&filetype) ? &filetype . ' ' . WebDevIconsGetFileTypeSymbol() : 'no ft') : ''
endfunction 

function! DeviconsOnly() 
  return winwidth(0) > 70 ? (strlen(&filetype) ? WebDevIconsGetFileTypeSymbol(): 'no ft') : ''
endfunction 

function! TabNum(n) abort 
  return a:n." \ue0bb"
endfunction 

function! Testing(n) abort
  let fullpath = expand('%:p')
  let arr = split(fullpath, '/' )
  let new = join(arr[-3:-1], '/')
  let winnr = tabpagewinnr(n)
  echo expand
  echo fnamemodify(getcwd(tabpagewinnr(a:n), a:n), '%:p')
  " return '/'.new
endfunction

function! TabPath(n) abort
  let buflist = tabpagebuflist(a:n)
  let winnr = tabpagewinnr(a:n)
  let _ = expand('#'.buflist[winnr - 1].':p')

  let arr = split(_, '/' )
  let new = join(arr[-3:-1], '/')
  return new !=# '' ? new : '[No Name]'
endfunction


function! LightlineFugitive() abort
  if &filetype ==# 'help'
    return ''
  endif
  try
    let head = "\ufb2b " 
    let branch = fugitive#head()
    let output = head.branch 

    if exists('*FugitiveHead')
      return output
    else
      return 
    endif
  catch
  endtry
  return ''
endfunction
"IBus
" let g:ibus_engine = 'English | ' 
" function! IBusOff()
"   let g:ibus_engine = 'English | '
" endfunction
" function! IBusOn()
"   let l:current_engine = system('ibus engine')
"   if l:current_engine !~? 'xkb:us::eng'
"     let g:ibus_engine = 'Vietnamese | '
"   endif
" execute 'silent !' . 'ibus engine ' . g:ibus_prev_engine
" endfunction
" augroup IBusHandler
"     autocmd CmdLineEnter [/?] call IBusOn()
"     autocmd CmdLineLeave [/?] call IBusOff()
" autocmd InsertEnter * call lightline#update()
" autocmd InsertLeave * call lightline#update()
" augroup END
" call IBusOff()

function! LightlineFugitiveForTab() abort
  let head = "\ufb2b " 
  " let g:ibus_engine = 'English | ' 
  " if mode() == 'i' 
  "   let l:current_engine = system('ibus engine')
  "   if l:current_engine =~ 'xkb:us::eng'
  "      let g:ibus_engine = 'English | '
  "   elseif l:current_engine =~ 'BambooUs'
  "      let g:ibus_engine = 'Vietnamese | '
  "   endif
  " elseif mode() == 'n' 
  "   let g:ibus_engine = 'English | '
  " endif
  let branch = fugitive#head()
  let output = head.branch
  if branch ==# ''
    return "\ue61b"
  else
    return output
  endif
endfunction
"}}}

" -----------------------------------------------------------------------------
"                                  @matchup 
" -----------------------------------------------------------------------------

" {{{
":hi MatchParen ctermbg=blue guibg=lightblue cterm=italic gui=italic
let g:loaded_matchit = 1
:hi MatchWord ctermfg=red guifg=LightSalmon cterm=underline gui=underline
"}}}

" -----------------------------------------------------------------------------
"                                  @maximizer 
" -----------------------------------------------------------------------------

" {{{
" nnoremap <silent><leader>m: MaximizerToggle<CR>
" vnoremap <silent><leader>m: MaximizerToggle<CR>gv
" inoremap <silent><leader>m: MaximizerToggle<CR>
let g:maximizer_default_mapping_key = '<leader>mx'
"}}}


" -----------------------------------------------------------------------------
"                                  @movelines 
" -----------------------------------------------------------------------------
" {{{
execute "set <M-k>=\ek"
execute "set <M-j>=\ej"
execute "set <M-h>=\eh"
execute "set <M-l>=\el"
nnoremap <silent> <A-j> :call MoveLineNormal("d")<CR>
nnoremap <silent> <A-k> :call MoveLineNormal("u")<CR>
" nnoremap <silent> <A-h> :call MoveLineNormal("l")<CR>
" nnoremap <silent> <A-l> :call MoveLineNormal("r")<CR>
xnoremap <silent> <A-j> :call MoveLinesVisual("down")<CR>

xnoremap <silent> <A-k> :call MoveLinesVisual("up")<CR>
" xnoremap <silent> <A-h> :call MoveLinesVisual("left")<CR>
" xnoremap <silent> <A-l> :call MoveLinesVisual("Right")<CR>
" }}}

" -----------------------------------------------------------------------------
"                                  @my-glyph-palette 
" -----------------------------------------------------------------------------

"{{{
augroup my-glyph-palette
  autocmd! *
  autocmd FileType startify call glyph_palette#apply()
  autocmd FileType fern call glyph_palette#apply()
augroup END
"}}}


" -----------------------------------------------------------------------------
"                                  @pingcursor 
" -----------------------------------------------------------------------------
let g:ping_cursor_flash_milliseconds = 250
nnoremap <S-w> :PingCursor<cr>


" -----------------------------------------------------------------------------
"                                  @quick-scope 
" -----------------------------------------------------------------------------
" Trigger a highlight only when pressing f and F.
let g:qs_highlight_on_keys = ['f', 'F']


" -----------------------------------------------------------------------------
"                                  @rainbow parentheses 
" -----------------------------------------------------------------------------

" {{{
" Activate
" Deactivate
":RainbowParentheses!
" Toggle
":RainbowParentheses!!
" See the enabled colors
":RainbowParenthesesColors
"let g:rainbow#max_level = 16
"let g:rainbow#pairs = [['(', ')'], ['[', ']']]
let g:rainbow_active = 1
let g:rainbow_conf = {
      \	'guifgs': ['MediumTurquoise', 'coral', 'MediumSpringGreen', 'Gold1'],
      \	'ctermfgs': ['lightblue', 'lightyellow', 'lightcyan', 'lightmagenta'],
      \}
"}}}


" -----------------------------------------------------------------------------
"                                  @sideways 
" -----------------------------------------------------------------------------
nnoremap swh :SidewaysLeft<cr>
nnoremap swl :SidewaysRight<cr>


" -----------------------------------------------------------------------------
"                                  @splitjoin 
" -----------------------------------------------------------------------------
"
let g:splitjoin_html_attributes_bracket_on_new_line = 1  
let g:splitjoin_curly_brace_padding = 1

" -----------------------------------------------------------------------------
"                                  @startify 
" -----------------------------------------------------------------------------

" {{{
execute "set <M-s>=\es"
nmap <M-s> :Startify<cr>

let g:startify_bookmarks = [
      \{ 'd': '/home/ndk2020/Work/doantotnghiep/OnlineExam2/'},    
      \{ 'c': '/home/ndk2020/Work/CPP/test.cpp'},    
      \{ 'w': '/home/ndk2020/Work/my_vimwiki/'},    
      \{ 'K': '/home/ndk2020/.config/kitty/kitty.conf'},    
      \{ 'S': '/home/ndk2020/.config/kitty/startup.conf'},    
      \{ 's': '/home/ndk2020/.config/starship.toml'},    
      \{ 'v': '~/.vimrc'},    
      \{ 't': '~/.tmux.conf'}    
      \ ]

let g:startify_lists = [
      \{ 'header': ['Bookmarks'], 'type': 'bookmarks'},    
      \{ 'header': ['MRU'], 'type': 'files'},    
      \{ 'header': ['MRU'.getcwd()], 'type': 'files'},    
      \ ]
let g:startify_custom_header = [
      \ '           ______           ______                      _            ',
      \ '           |  _  \          | ___ \                    | |           ',
      \ '           | | | |___ _ __  | |_/ /___ _ __   ___  __ _| |_ ___ _ __ ',
      \ '           | | | / _ \ .__| |    // _ \ ._ \ / _ \/ _` | __/ _ \ .__|',
      \ '           | |/ /  __/ |    | |\ \  __/ |_) |  __/ (_| | ||  __/ |   ',
      \ '           |___/ \___|_|    \_| \_\___| .__/ \___|\__,_|\__\___|_|   ',
      \ '                                      | |                            ',
      \ '                                      |_|       ',
      \]
"}}}


" -----------------------------------------------------------------------------
"                                  @ultinsips, coc-snippets 
" -----------------------------------------------------------------------------

"{{{
" let g:UltiSnipsJumpForwardTrigger="<c-b>
" let g:UltiSnipsJumpBackwardTrigger="<c-z>"
" let g:UltiSnipsEditSplit="vertical"
" let g:UltiSnipsExpandTrigger="<c-f1>"
"au FileType c UltiSnipsAddFiletypes cpp.c


" Use <C-j> for jump to next placeholder, it's default of coc.nvim
let g:coc_snippet_next = '<c-b>'
" Use <C-k> for jump to previous placeholder, it's default of coc.nvim
let g:coc_snippet_prev = '<c-z>'
"}}}


" -----------------------------------------------------------------------------
"                                  @tagbar 
" -----------------------------------------------------------------------------

" {{{
"nnoremap <F9> :TagbarToggle<CR>
let g:tagbar_type_typescript = {
      \ 'ctagstype': 'typescript',
      \ 'kinds': [
        \ 'c:classes',
        \ 'n:modules',
        \ 'f:functions',
        \ 'v:variables',
        \ 'v:varlambdas',
        \ 'm:members',
        \ 'i:interfaces',
        \ 'e:enums',
        \ ]
        \ }
"}}}

" -----------------------------------------------------------------------------
"                                  @vim-visual-multi 
" -----------------------------------------------------------------------------

"{{{
let g:VM_maps = {}
" let g:VM_maps['Find Under']         = '<C-d>'
" let g:VM_maps['Find Subword Under'] = '<C-d>'
" let g:VM_maps["Select Cursor Down"] = '<M-C-Down>'
" let g:VM_maps["Select Cursor Up"]   = '<M-C-Up>'
let g:VM_maps["Mouse Cursor"]   = '<C-LeftMouse>'
let g:VM_silent_exit = 1
let g:VM_theme = 'iceblue'
"}}}


" -----------------------------------------------------------------------------
"                                  @vim-wiki 
" -----------------------------------------------------------------------------

" {{{
"C:\Users\KhoaN\OneDrive\Documents\"- setting
" let wiki_1 = {}
" let wiki_1.path = '~/vimwiki/'
" let wiki_1.path_html = '~/vimwiki_html/'

let wiki_docs = {}
let wiki_docs.path = '/home/ndk2020/Work/my_vimwiki/'
let wiki_docs.path_html = '/home/ndk2020/Work/my_vimwiki/Export'
let wiki_docs.syntax = 'markdown'
let wiki_docs.ext = '.md'
let g:vimwiki_list = [wiki_docs]

let g:vimwiki_markdown_link_ext = 1
let g:vimwiki_ext2syntax = {
      \ '.md': 'markdown',
      \ '.mkd': 'markdown',
      \ '.markdown': 'markdown',
      \ '.mdown': 'markdown',
      \ '.wiki': 'media'
      \}"}}}


" -----------------------------------------------------------------------------
"                                  @vimspector 
" -----------------------------------------------------------------------------

"{{{
execute "set <M-o>=\eo"
execute "set <M-i>=\ei"
execute "set <M-O>=\eO"
let g:vimspector_enable_mappings = 'HUMAN'
" nnoremap <leader>s  : call vimspector#Launch()<CR>
" nnoremap <leader>sc : call GotoWindow(g:vimspector_session_windows.code)<CR>
" nnoremap <leader>sv : call GotoWindow(g:vimspector_session_windows.variables)<CR>
" nnoremap <leader>sw : call GotoWindow(g:vimspector_session_windows.watches)<CR>
" nnoremap <leader>ss : call GotoWindow(g:vimspector_session_windows.stack_trace)<CR>
" nnoremap <leader>so : call GotoWindow(g:vimspector_session_windows.output)<CR>
" nnoremap <leader>sx : call vimspector#Reset()<CR>
" nnoremap <leader>sX : call vimspector#ClearBreakpoints()<CR>
" nnoremap <S-k> :call vimspector#StepOut()<CR>
" nnoremap <S-l> :call vimspector#StepInto()<CR>
" nnoremap <S-j> :call vimspector#StepOver()<CR>
" nnoremap <leader>s_  : call vimspector#Restart()<CR>
" nnoremap <leader>sn  : call vimspector#Continue()<CR>
" nnoremap <leader>src : call vimspector#RunToCursor()<CR>
" nnoremap <leader>sb  : call vimspector#ToggleBreakpoint()<CR>
" nnoremap <leader>se  : call vimspector#ToggleConditionalBreakpoint()<CR>
" }}}


" -----------------------------------------------------------------------------
"                                  @coc-nvim 
" -----------------------------------------------------------------------------

" {{{
" !!! Things to do after first time installing !!!
":CocList extensions 
":CocList marketplace
" extensions for web dev
":CocInstall coc-marketplace
":CocInstall coc-tsserver coc-json coc-html coc-css
" let g:coc_global_extensions = [
"     \ 'coc-tsserver',
"     \ 'coc-css',
"     \ 'coc-html',
"     \ 'coc-json',
"     \ 'coc-marketplace'
"     \ 'coc-prettier'
"     \ 'coc-snippets'
"     \ ]
" !!! End of Things to do after first time installing !!!

autocmd FileType json syntax match Comment +\/\/.\+$+
set updatetime=300
set shortmess+=c

if has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif
"highlight SignColumn guibg=#272822

" !!! map keyboard to navigate up/down in completion list !!!
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction

inoremap <silent><expr> <Tab>
      \ pumvisible() ? "\<Down>" :
      \ <SID>check_back_space() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<up>" : "\<c-h>"

" Use <Tab> and <S-Tab> to navigate the completion list
" inoremap <expr> <Tab> pumvisible() ? "\<Down>" : "\<Tab>"
" inoremap <expr> <S-Tab> pumvisible() ? "\<Up>" : "\<S-Tab>"

if exists('*complete_info')

  inoremap <silent><expr> <cr> complete_info(['selected'])['selected'] != -1 ? "\<C-y>" : "\<C-g>u\<CR>"
  imap <CR> <CR><Plug>(EnwiseClose)

endif
" !!! end of map keyboard to navigate up/down in completion list !!!

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif


nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
nnoremap <silent><nowait> <space>d :call CocAction('jumpDefinition', v:false)<CR>

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>
function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
highlight CocHighlightText  guibg=#5c527f
autocmd CursorHold * silent call CocActionAsync('highlight')
highlight CocUnusedHighlight guifg=#FFF9B2
"autocmd ColorScheme * highlight CocHighlightText ctermfg=red guifg=red guibg=red
"#5C527F
"#3B14A7
"#005F99
"#1e6f5c
"#52057b
"#2c3b41

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

"coc-prettier setting
command! -nargs=0 Prettier :CocCommand prettier.formatFile
" vmap <leader>f  <Plug>(coc-format-selected)
" nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region 
"nmap <leader>a  <Plug>(coc-codeaction-selected)<CR>
"xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)<cr>
" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)<cr>
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Remap <C-f> and <C-b> for scroll float windows/popups.
if has('nvim-0.4.0') || has('patch-8.2.0750')
  nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
  vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
endif

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of language server.
" nmap <silent> <C-s> <Plug>(coc-range-select)
" xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Mappings for CoCList
" Show all diagnostics.
"nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
"nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
"nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
"nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
"nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
"nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
"nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>
" Mappings for coc-fzf
nnoremap <silent> <space><space> :<C-u>CocFzfList<CR>
nnoremap <silent> <space>a       :<C-u>CocFzfList diagnostics<CR>
nnoremap <silent> <space>b       :<C-u>CocFzfList diagnostics --current-buf<CR>
nnoremap <silent> <space>c       :<C-u>CocFzfList commands<CR>
nnoremap <silent> <space>e       :<C-u>CocFzfList extensions<CR>
nnoremap <silent> <space>l       :<C-u>CocFzfList location<CR>
nnoremap <silent> <space>o       :<C-u>CocFzfList outline<CR>
nnoremap <silent> <space>s       :<C-u>CocFzfList symbols<CR>
nnoremap <silent> <space>p       :<C-u>CocFzfListResume<CR>


"Coc-Explorer Setting
":nnoremap <space>e :CocCommand explorer<CR>

"Coc-multi-cursor Setting
" nmap <silent> <C-c> <Plug>(coc-cursors-position)
" nmap <silent> <C-d> <Plug>(coc-cursors-word)
" xmap <silent> <C-d> <Plug>(coc-cursors-range)
" use normal command like `<leader>xi(`
" nmap <leader>x  <Plug>(coc-cursors-operator)
"
"Coc-fzf-preview settings
" The theme used in the bat preview
"$FZF_PREVIEW_PREVIEW_BAT_THEME = 'ansi'
let g:fzf_preview_command = 'bat --color=always --theme="gruvbox-dark" {-1}'
"FzfPreviewVistaBufferCtags does not work Vista must be initialized. 
"Run the Vista command once or write the following settings.
autocmd VimEnter * call vista#RunForNearestMethodOrFunction()


nmap <Leader>f [fzf-p]
xmap <Leader>f [fzf-p]

nnoremap <silent> [fzf-p]p     :<C-u>CocCommand fzf-preview.FromResources project_mru git<CR>
nnoremap <silent> [fzf-p]gs    :<C-u>CocCommand fzf-preview.GitStatus<CR>
nnoremap <silent> [fzf-p]ga    :<C-u>CocCommand fzf-preview.GitActions<CR>
nnoremap <silent> [fzf-p]b     :<C-u>CocCommand fzf-preview.Buffers<CR>
nnoremap <silent> [fzf-p]B     :<C-u>CocCommand fzf-preview.AllBuffers<CR>
nnoremap <silent> [fzf-p]o     :<C-u>CocCommand fzf-preview.FromResources buffer project_mru<CR>
nnoremap <silent> [fzf-p]<C-o> :<C-u>CocCommand fzf-preview.Jumps<CR>
nnoremap <silent> [fzf-p]g;    :<C-u>CocCommand fzf-preview.Changes<CR>
nnoremap <silent> [fzf-p]/     :<C-u>CocCommand fzf-preview.Lines --add-fzf-arg=--no-sort --add-fzf-arg=--query="'"<CR>
nnoremap <silent> [fzf-p]*     :<C-u>CocCommand fzf-preview.Lines --add-fzf-arg=--no-sort --add-fzf-arg=--query="'<C-r>=expand('<cword>')<CR>"<CR>
nnoremap          [fzf-p]gr    :<C-u>CocCommand fzf-preview.ProjectGrep<Space>
xnoremap          [fzf-p]gr    "sy:CocCommand   fzf-preview.ProjectGrep<Space>-F<Space>"<C-r>=substitute(substitute(@s, '\n', '', 'g'), '/', '\\/', 'g')<CR>"
nnoremap <silent> [fzf-p]t     :<C-u>CocCommand fzf-preview.BufferTags<CR>
nnoremap <silent> [fzf-p]q     :<C-u>CocCommand fzf-preview.QuickFix<CR>
nnoremap <silent> [fzf-p]l     :<C-u>CocCommand fzf-preview.LocationList<CR>
"}}}


" =============================================================================
"                          End of Plugin Setting Section
" =============================================================================
